DESCRIPTION = "Chromium Embedded Framework. A simple framework for embedding chromium browser windows in other applications"
LICENSE = "BSD"
LIC_FILES_CHKSUM = "file://${WORKDIR}/chromium/src/LICENSE;md5=d2d164565cc10f298390174d9cb6d18d"
DEPENDS = "xz-native pciutils pulseaudio xextproto cairo nss gtk+ zlib-native libxi libxss cups ninja-native gconf"
DEPENDS += "libgnome-keyring gtkglext"
SRC_URI = "\
        svn://src.chromium.org/svn/;rev=242709;module=trunk/tools/depot_tools;protocol=http \
        file://include.gypi \
        file://oe-defaults.gypi \
        file://000_OE_GypIncludes.patch \
"

S = "${WORKDIR}/chromium"

# The following information can be obtained at
# http://code.google.com/p/chromiumembedded/source/browse/branches/1650/cef3/CHROMIUM_BUILD_COMPATIBILITY.txt
CHROMIUM_VERSION = "31.0.1650.57"
CEF3_BRANCH = "${PV}"
CEF3_REVISION = "1531"

PR = "${CEF3_REVISION}"


# include.gypi exists only for armv6 and armv7a and there isn't something like COMPATIBLE_ARCH afaik
COMPATIBLE_MACHINE = "(-)"
COMPATIBLE_MACHINE_i586 = "(.*)"
COMPATIBLE_MACHINE_x86-64 = "(.*)"
COMPATIBLE_MACHINE_armv6 = "(.*)"
COMPATIBLE_MACHINE_armv7a = "(.*)"
COMPATIBLE_MACHINE_omap5 = "(.*)"

inherit gettext

EXTRA_OEGYP =	" \
	${@base_contains('DISTRO_FEATURES', 'ld-is-gold', '', '-Dlinux_use_gold_binary=0', d)} \
	${@base_contains('DISTRO_FEATURES', 'ld-is-gold', '', '-Dlinux_use_gold_flags=0', d)} \
	-I ${WORKDIR}/oe-defaults.gypi \
	-I ${WORKDIR}/include.gypi \
	-f ninja \
"
ARMFPABI_armv7a = "${@bb.utils.contains('TUNE_FEATURES', 'callconvention-hard', 'arm_float_abi=hard', 'arm_float_abi=softfp', d)}"
ARMFPABI_omap5 = "${@bb.utils.contains('TUNE_FEATURES', 'callconvention-hard', 'arm_float_abi=hard', 'arm_float_abi=softfp', d)}"

do_checkout_chromium_sources() {
	export PATH=${WORKDIR}/trunk/tools/depot_tools/:$PATH
	mkdir -p ${WORKDIR}/chromium
	cd ${WORKDIR}/chromium
	# http://www.magpcss.org/ceforum/viewtopic.php?f=6&t=11267
	# svn list https://cld2.googlecode.com/svn/trunk
	# ^^^^^
	${WORKDIR}/trunk/tools/depot_tools/gclient config http://src.chromium.org/svn/releases/${CHROMIUM_VERSION}
	${WORKDIR}/trunk/tools/depot_tools/gclient sync --jobs 8 --force
	cd ${WORKDIR}/chromium/src
	svn co http://chromiumembedded.googlecode.com/svn/branches/${CEF3_BRANCH}/cef3@${CEF3_REVISION} cef
}
addtask checkout_chromium_sources before do_patch

export GYP_DEFINES="${ARMFPABI} release_extra_cflags='-Wno-error=unused-local-typedefs' sysroot=''"

do_configure() {
	cd ${WORKDIR}/chromium/src
	# replace LD with CXX, to workaround a possible gyp issue?
	LD="${CXX}" export LD
	CC="${CC}" export CC
	CXX="${CXX}" export CXX
	CC_host="gcc" export CC_host
	CXX_host="g++" export CXX_host
	export GYP_GENERATORS='ninja'
	CEF_OE_GYPI="${WORKDIR}/oe-defaults.gypi" export CEF_OE_GYPI
	CEF_OE_INCLUDE_GYPI="${WORKDIR}/include.gypi" export CEF_OE_INCLUDE_GYPI
	cd ${WORKDIR}/chromium/src/cef
	export PATH=${WORKDIR}/trunk/tools/depot_tools/:$PATH
	./cef_create_projects.sh
}

do_compile() {
	# build with ninja
	cd ${WORKDIR}/chromium/src
	ninja -C out/Debug cefclient cef_unittests -j 4
	ninja -C out/Release cefclient cef_unittests -j 4
	cd ${WORKDIR}/chromium/src/cef/tools
	export PATH=${WORKDIR}/trunk/tools/depot_tools/:$PATH
	./make_distrib.sh --ninja-build
	# Build the distrib package to create the final binaries.
	cd ${WORKDIR}/chromium/src/cef/binary_distrib/cef_binary_3.${CEF3_BRANCH}.${CEF3_REVISION}_linux32
	./build.sh Debug
	./build.sh Release
}

do_install() {
	install -d ${D}/usr/include/cef3
	install -d ${D}/opt/cef3
	install -d ${D}/opt/cef3/.debug

	cd ${WORKDIR}/chromium/src/cef/binary_distrib/cef_binary_3.${CEF3_BRANCH}.${CEF3_REVISION}_linux32
	cp -rf include/* ${D}/usr/include/cef3


	install -m 0755 out/Release/libcef.so ${D}/opt/cef3
	install -m 0755 out/Release/libffmpegsumo.so ${D}/opt/cef3
	install -m 0755 out/Release/cefclient ${D}/opt/cef3
	cp -rf out/Release/files ${D}/opt/cef3
	cp -rf out/Release/locales ${D}/opt/cef3
	install -m 0755 out/Release/obj.target/libcef_dll_wrapper.a ${D}/opt/cef3

	install -m 0755 out/Debug/libcef.so ${D}/opt/cef3/.debug
	install -m 0755 out/Debug/libffmpegsumo.so ${D}/opt/cef3/.debug
	install -m 0755 out/Debug/cefclient ${D}/opt/cef3/.debug
#	install -m 0755 out/Debug/obj.target/libcef_dll_wrapper.a ${D}/opt/cef3/.debug


}

FILES_${PN}-staticdev = "/opt/cef3/libcef_dll_wrapper.a"
FILES_${PN}-dbg = "/opt/cef3/.debug"
FILES_${PN} = "/opt/cef3"

FILES_${PN}-dev = "/usr/include/cef3"



